package linface

import "net"

// server的抽象接口

// 定义服务器的接口
type IServer interface {
	// 启动服务器
	Start()
	// 停止服务器
	Stop()
	// 开启业务服务器
	Serve()
	// 查看服务器状态
	Status()
	// 路由
	AddRouter(router IRouter)
}

// 定义连接接口
type IConnection interface {
	// 启动连接，让当前连接开始工作
	Start()
	// 停止连接，结束当前的连接状态M
	Stop()
	// 从当前连接获取原始的socket TCPConn
	GetTCPConnection() *net.TCPConn
	// 获取当前连接的ID
	GetConnID() uint32
	// 获取远程客户端的地址信息
	RemoteArr() net.Addr
}

// 定义一个统一处理链接业务的接口
type HandFunc func(*net.TCPConn, []byte, int) error
