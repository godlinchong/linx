package linet

import (
	"linx/v0.4/linface"
)

type Request struct {
	conn linface.IConnection // 已经和客户端建立好的链接
	data []byte              // 客户端请求数据
}

func (r *Request) GetConnection() linface.IConnection {
	return r.conn
}

func (r *Request) GetData() []byte {
	return r.data
}
