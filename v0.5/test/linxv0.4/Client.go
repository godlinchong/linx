package main

import (
	"fmt"
	"linx/v0.4/linet"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:4444")
	if err != nil {
		fmt.Println("client dial err:", err)
		return
	}
	// 创建一个封装对象 dp
	dp := linet.NewDataPack()
	msg1 := &linet.Message{
		Id:      0,
		DataLen: 5,
		Data:    []byte{'h', 'e', 'l', 'l', 'o'},
	}
	sendData1, err := dp.Pack(msg1)
	if err != nil {
		fmt.Println("client pack msg1 err:", err)
		return
	}

	msg2 := &linet.Message{
		Id:      0,
		DataLen: 5,
		Data:    []byte{'k', 'e', 'l', 'l', 'o'},
	}

	sendData2, err := dp.Pack(msg2)
	if err != nil {
		fmt.Println("client pack msg1 err:", err)
		return
	}
	//将sendData1，和 sendData2 拼接一起，组成粘包
	sendData1 = append(sendData1, sendData2...)
	conn.Write(sendData1)
	select {}
}
