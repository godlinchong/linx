package main

import (
	"fmt"
	"linx/v0.5/linet"
	"linx/v0.5/linface"
)

type PingRouter struct {
	linet.BaseRouter //一定要先基础BaseRouter
}

////Test PreHandle
//func (this *PingRouter) PreHandle(request linface.IRequest) {
//	fmt.Println("Call Router PreHandle")
//	_, err := request.GetConnection().GetTCPConnection().Write([]byte("before ping ....\n"))
//	if err != nil {
//		fmt.Println("call back ping ping ping error")
//	}
//}

//Test Handle
func (this *PingRouter) Handle(request linface.IRequest) {
	fmt.Println("Call PingRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping
	fmt.Println("recv from client : msgId=", request.GetMsgID(),
		", data=", string(request.GetData()))
	//回写数据
	err := request.GetConnection().SendMsg(1, []byte("ping...ping...ping"))
	if err != nil {
		fmt.Println(err)
	}
}

////Test PreHandle
//func (this *PingRouter) PostHandle(request linface.IRequest) {
//	fmt.Println("Call Router PostHandle")
//	_, err := request.GetConnection().GetTCPConnection().Write([]byte("after ping ....\n"))
//	if err != nil {
//		fmt.Println("call back ping ping ping error")
//	}
//}

//

func main() {
	fmt.Println("Server starting ......")
	s := linet.NewServer("[linx V0.5]")
	s.AddRouter(&PingRouter{})
	s.Serve()
}
