package main

import (
	"fmt"
	"linx/v0.6/linet"
	"linx/v0.6/linface"
)

type PingRouter struct {
	linet.BaseRouter //一定要先基础BaseRouter
}

//Test Handle
func (this *PingRouter) Handle(request linface.IRequest) {
	fmt.Println("Call PingRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping
	fmt.Println("recv from client : msgId=", request.GetMsgID(),
		", data=", string(request.GetData()))
	//回写数据
	err := request.GetConnection().SendMsg(1, []byte("ping...ping...ping"))
	if err != nil {
		fmt.Println(err)
	}
}

//HelloZinxRouter Handle
type HelloZinxRouter struct {
	linet.BaseRouter
}

func (this *HelloZinxRouter) Handle(request linface.IRequest) {
	fmt.Println("Call HelloZinxRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping 6.3 使用Zinx-V0.6完成应用程序 90
	fmt.Println("recv from client : msgId=", request.GetMsgID(),
		", data=", string(request.GetData()))
	err := request.GetConnection().SendMsg(1, []byte("Hello ZinxRouter V0.6"))
	if err != nil {
		fmt.Println(err)
	}
}
func main() {
	fmt.Println("Server starting ......")
	s := linet.NewServer("[linx v0.6]")
	//配置路由
	s.AddRouter(0, &PingRouter{})
	s.AddRouter(1, &HelloZinxRouter{})
	s.Serve()
}
