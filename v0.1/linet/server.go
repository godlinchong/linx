package linet

import (
	"fmt"
	"linx/v0.1/common"
	"linx/v0.1/linface"
	"net"
	"time"
)

// iServer 接口实现，定义一个server服务类
type Server struct {
	// 名称
	Name string
	// tcp4 or tcp6
	IPVersion string
	//ip
	IP string
	// port
	Port int
}

func (s *Server) Start() {
	fmt.Printf("[Start] Server listenner at IP: "+
		"%s,Port %d is staring\n", s.IP, s.Port)
	// 开启一个gorouties去做监听
	go func() {
		// 1 获取一个tcp的Addr
		addr, err := net.ResolveTCPAddr(s.IPVersion, fmt.Sprintf("%s:%d", s.IP, s.Port))
		common.CheckErr(err, "resolve tcp addr err:")
		// 监听服务器地址
		listenner, err := net.ListenTCP(s.IPVersion, addr)
		common.CheckErr(err, "listen TCP err:")
		// 2.已经成功监控
		fmt.Println("start Linx Server ", s.Name, " succ,now listening ...")
		// 3.启动server网络连接业务
		for {
			conn, err := listenner.AcceptTCP()
			if err != nil {
				fmt.Println("Accept err ", err)
				continue
			}
			//我们这里暂时做一个最大512字节的回显服务
			go func() {
				for {
					buf := make([]byte, 512)
					cnt, err := conn.Read(buf)
					fmt.Printf("server call back :%s,cnt=%d\n", buf, cnt)
					if err != nil {
						fmt.Println("recv buf err ", err)
						continue
					}
					// 回显
					if _, err := conn.Write(buf[:cnt]); err != nil {
						fmt.Println("write back buf err ", err)
						continue
					}

				}
			}()

		}
	}()
}

func (s *Server) Stop() {
	fmt.Println("[Stop] Linx Server, name:", s.Name)
}

func (s *Server) Serve() {
	s.Start()
	for {
		time.Sleep(10 * time.Second)
	}

}

func (s *Server) Status() {
	fmt.Println("TDD status")
}

func NewServer(name string) linface.IServer {
	s := &Server{
		Name:      name,
		IPVersion: "tcp4",
		IP:        "localhost",
		Port:      4444,
	}
	return s
}
