package linet

import (
	"fmt"
	"linx/v0.8/common"
	"linx/v0.8/linface"
	"net"
	"time"
)

// iServer 接口实现，定义一个server服务类
type Server struct {
	// 名称
	Name string
	// tcp4 or tcp6
	IPVersion string
	//ip
	IP string
	// port
	Port int
	//当前Server的消息管理模块，用来绑定MsgId和对应的处理方法
	msgHandler linface.IMsgHandle
	// 连接管理器
	ConnMgr     linface.IConnManager
	OnConnStart func(connection linface.IConnection)
	//该Server的连接断开时的Hook函数
	OnConnStop func(connection linface.IConnection)
}

func (s *Server) Start() {
	fmt.Printf("[Start] Server listenner at IP: "+
		"%s,Port %d is staring\n", s.IP, s.Port)
	fmt.Printf("[Zinx] Version: %s, MaxConn: %d, MaxPacketSize:%d\n",
		common.GlobalObject.Version,
		common.GlobalObject.MaxConn,
		common.GlobalObject.MaxPacketSize)
	// 开启一个gorouties去做监听
	go func() {
		//0 启动worker工作池机制
		s.msgHandler.StartWorkerPool()
		// 1 获取一个tcp的Addr
		addr, err := net.ResolveTCPAddr(s.IPVersion, fmt.Sprintf("%s:%d", s.IP, s.Port))
		common.CheckErr(err, "resolve tcp addr err:")
		// 监听服务器地址
		listenner, err := net.ListenTCP(s.IPVersion, addr)
		common.CheckErr(err, "listen TCP err:")
		// 2.已经成功监控
		fmt.Println("start Linx Server ", s.Name, " succ,now listening ...")
		// 3.启动server网络连接业务
		// ConnID生成
		var cid uint32
		cid = 0
		for {
			// 阻塞等待客户端建立连接请求
			conn, err := listenner.AcceptTCP()
			if err != nil {
				fmt.Println("Accept err ", err)
				continue
			}
			//设置服务器最大连接控制,如果超过最大连接，那么则关闭此新的连接
			if s.ConnMgr.Len() > common.GlobalObject.MaxConn {
				_ = conn.Close()
				continue
			}
			// 处理连接的业务请求方法,此时应该有handler 和 conn是绑定的
			dealConn := NewConnection(s, conn, cid, s.msgHandler)
			cid++
			// 启动当前链接的处理业务
			go dealConn.Start()

		}
	}()
}

func (s *Server) Stop() {
	fmt.Println("[Stop] Linx Server, name:", s.Name)
	//将其他需要清理的连接信息或者其他信息 也要一并停止或者清理
	s.ConnMgr.ClearConn()
}

func (s *Server) Serve() {
	s.Start()
	for {
		time.Sleep(10 * time.Second)
	}

}

//设置该Server的连接创建时Hook函数
func (s *Server) SetOnConnStart(hookFunc func(linface.IConnection)) {
	s.OnConnStart = hookFunc
}

//设置该Server的连接断开时的Hook函数
func (s *Server) SetOnConnStop(hookFunc func(linface.IConnection)) {
	s.OnConnStop = hookFunc
}

func (s *Server) Status() {
	fmt.Println("TDD status")
}

//调用连接OnConnStart Hook函数
func (s *Server) CallOnConnStart(conn linface.IConnection) {
	if s.OnConnStart != nil {
		fmt.Println("---> CallOnConnStart....")
		s.OnConnStart(conn)
	}
}

//调用连接OnConnStop Hook函数
func (s *Server) CallOnConnStop(conn linface.IConnection) {
	if s.OnConnStop != nil {
		fmt.Println("---> CallOnConnStop....")
		s.OnConnStop(conn)
	}
}

func (s *Server) GetConnMgr() linface.IConnManager {
	return s.ConnMgr
}

func (s *Server) AddRouter(msgId uint32, router linface.IRouter) {
	s.msgHandler.AddRouter(msgId, router)
	fmt.Println("msgId:", msgId, ",router:", router)
}

func NewServer(name string) linface.IServer {
	s := &Server{
		Name:       name,
		IPVersion:  "tcp4",
		IP:         "localhost",
		Port:       4444,
		msgHandler: NewMsgHandle(),
		ConnMgr:    NewConnManager(),
	}
	return s
}
