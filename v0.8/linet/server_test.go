package linet

import (
	"fmt"
	"net"
	"testing"
	"time"
)

func ClientTest() {
	fmt.Println("Client Test ..... start")
	time.Sleep(3 * time.Second)
	conn, err := net.Dial("tcp", "localhost:4444")
	if err != nil {
		fmt.Println("client start err,exit!")
		return
	}
	for {
		_, err := conn.Write([]byte("hello world!"))
		if err != nil {
			fmt.Println("write err ", err)
			return
		}
		buf := make([]byte, 512)
		cnt, err := conn.Read(buf)
		if err != nil {
			fmt.Println("read buf err")
			return
		}
		fmt.Printf("server call back:%s, cnt = %d\n", buf, cnt)
	}

}

func TestServer_Start(t *testing.T) {
	s := NewServer("[linx v0.1]")
	go ClientTest()
	s.Serve()
}
