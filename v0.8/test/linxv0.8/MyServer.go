package main

import (
	"fmt"
	"linx/v0.8/linet"
	"linx/v0.8/linface"
)

type PingRouter struct {
	linet.BaseRouter //一定要先基础BaseRouter
}

//Test Handle
func (this *PingRouter) Handle(request linface.IRequest) {
	fmt.Println("Call PingRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping
	fmt.Println("recv from client : msgId=", request.GetMsgID(),
		", data=", string(request.GetData()))
	//回写数据
	err := request.GetConnection().SendBuffMsg(request.GetMsgID(), []byte("ping...ping...ping"))
	if err != nil {
		fmt.Println(err)
	}
}

//HelloZinxRouter Handle
type HelloZinxRouter struct {
	linet.BaseRouter
}

func (this *HelloZinxRouter) Handle(request linface.IRequest) {
	fmt.Println("Call HelloZinxRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping 6.3 使用Zinx-v0.8完成应用程序 90
	fmt.Println("recv from client : msgId=", request.GetMsgID(),
		", data=", string(request.GetData()))
	err := request.GetConnection().SendMsg(request.GetMsgID(), []byte("Hello ZinxRouter v0.8"))
	if err != nil {
		fmt.Println(err)
	}
}

//创建连接的时候执行
func DoConnectionBegin(conn linface.IConnection) {
	fmt.Println("DoConnecionBegin is Called ... ")
	err := conn.SendMsg(2, []byte("DoConnection BEGIN..."))
	if err != nil {
		fmt.Println(err)
	}
}

//连接断开的时候执行
func DoConnectionLost(conn linface.IConnection) {
	fmt.Println("DoConneciotnLost is Called ... ")
}

func main() {
	fmt.Println("Server starting ......")
	s := linet.NewServer("[linx v0.8]")

	//注册链接hook回调函数
	s.SetOnConnStart(DoConnectionBegin)
	s.SetOnConnStop(DoConnectionLost)

	//配置路由
	s.AddRouter(0, &PingRouter{})
	s.AddRouter(1, &HelloZinxRouter{})
	s.Serve()
}
