package main

import (
	"fmt"
	"linx/v0.3/linface"
)
import "linx/v0.3/linet"

type PingRouter struct {
	linet.BaseRouter //一定要先基础BaseRouter
}

//Test PreHandle
func (this *PingRouter) PreHandle(request linface.IRequest) {
	fmt.Println("Call Router PreHandle")
	_, err := request.GetConnection().GetTCPConnection().Write([]byte("before ping ....\n"))
	if err != nil {
		fmt.Println("call back ping ping ping error")
	}
}

//Test PreHandle
func (this *PingRouter) Handle(request linface.IRequest) {
	fmt.Println("Call Router Handle")
	_, err := request.GetConnection().GetTCPConnection().Write([]byte("ping ....\n"))
	if err != nil {
		fmt.Println("call back ping ping ping error")
	}
}

//Test PreHandle
func (this *PingRouter) PostHandle(request linface.IRequest) {
	fmt.Println("Call Router PostHandle")
	_, err := request.GetConnection().GetTCPConnection().Write([]byte("after ping ....\n"))
	if err != nil {
		fmt.Println("call back ping ping ping error")
	}
}

//

func main() {
	fmt.Println("Server starting ......")
	s := linet.NewServer("[linx V0.3]")
	s.AddRouter(&PingRouter{})
	s.Serve()
}
