package linface

/**
IRequest接口:
实际上是把客户端请求链接信息和请求数据包装到Request里
*/

type IRequest interface {
	GetConnection() IConnection //获取请求的链接信息
	GetData() []byte            // 获取请求的数据
}
