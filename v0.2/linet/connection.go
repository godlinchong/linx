package linet

import (
	"fmt"
	"linx/v0.1/linface"
	"log"
	"net"
)

type Connection struct {
	// 当前连接的socket TCP套接字
	Conn *net.TCPConn
	// 当前连接的id
	ConnID uint32
	// 当前链接的关闭状态
	isClosed bool
	// 该连接的处理方法api
	handleAPI linface.HandFunc
	// 告知改连接已经退出或者停止的channel
	ExitBuffChan chan bool
}

// 创建连接的方法
func NewConnection(conn *net.TCPConn, connID uint32, callback_api linface.HandFunc) *Connection {
	c := &Connection{
		Conn:         conn,
		ConnID:       connID,
		isClosed:     false,
		handleAPI:    callback_api,
		ExitBuffChan: make(chan bool, 1),
	}

	return c
}

// 处理conn读取数据的Goroutine
func (c *Connection) StartReader() {
	fmt.Println("Reader Goroutine is running ...")
	defer fmt.Println(c.RemoteArr().String(), " conn reader exit!")
	defer c.Stop()
	for {
		// 读取我们最大的数据到buf中
		buf := make([]byte, 512)
		cnt, err := c.Conn.Read(buf)
		if err != nil {
			fmt.Println("recv buf err", err)
			c.ExitBuffChan <- true
			continue
		}
		// 调用当前链接业务(这里执行的是当前conn绑定的handle方法)
		if err := c.handleAPI(c.Conn, buf, cnt); err != nil {
			log.Println("connID", c.ConnID, "handle is err")
			return
		}

	}
}
func (c *Connection) Start() {
	// 开启处理该连接读取到客户端数据之后的请求业务
	go c.StartReader()

	for {
		select {
		case <-c.ExitBuffChan:
			//得到突出的消息不在阻塞
			return
		}
	}

}

//获取远程客户端地址信息
func (c *Connection) RemoteArr() net.Addr {
	return c.Conn.RemoteAddr()
}

//停止连接，结束当前连接状态M
func (c *Connection) Stop() {
	fmt.Println("Conn Stop()...ConnID = ", c.ConnID)
	//如果当前链接已经关闭
	if c.isClosed == true {
		return
	}
	c.isClosed = true
	// 关闭socket链接
	c.Conn.Close()
	// 通知从缓冲队列读取数据的业务,该连接该链接已关闭
	c.ExitBuffChan <- true
	// 关闭改连接的全部管道
	close(c.ExitBuffChan)
}

//从当前链接获取原始的socket TCPConn
func (c *Connection) GetTCPConnection() *net.TCPConn {
	return c.Conn
}

func (c *Connection) GetConnID() uint32 {
	return c.ConnID
}
