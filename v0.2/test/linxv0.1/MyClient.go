package main

import (
	"fmt"
	"net"
	"time"
)

func main() {
	fmt.Println("Client Test Start .......")
	time.Sleep(3 * time.Second)
	con, err := net.Dial("tcp", "localhost:4444")
	if err != nil {
		fmt.Println("server conn err:", err)
		return
	}
	for {
		_, err := con.Write([]byte("hello world!"))
		if err != nil {
			fmt.Println("write error err:", err)
			return
		}
		buf := make([]byte, 512)
		cnt, err := con.Read(buf)
		if err != nil {
			fmt.Println("read buf error")
			return
		}
		fmt.Printf("server call back :%s,cnt=%d\n", buf, cnt)
		time.Sleep(1 * time.Second)

	}

}
