package main

import "fmt"
import "linx/v0.1/linet"

func main() {
	fmt.Println("Server starting ......")
	s := linet.NewServer("[linx V0.1]")
	s.Serve()
}
