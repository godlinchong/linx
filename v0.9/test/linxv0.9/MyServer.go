package main

import (
	"linx/v0.9/linet"
	"linx/v0.9/linface"
	"log"
)

type PingRouter struct {
	linet.BaseRouter //一定要先基础BaseRouter
}

//Test Handle
func (this *PingRouter) Handle(request linface.IRequest) {
	log.Println("Call PingRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping
	log.Println("recv from client : msgId=", request.GetMsgID(),
		", data=", string(request.GetData()))
	//回写数据
	err := request.GetConnection().SendBuffMsg(request.GetMsgID(), []byte("ping...ping...ping"))
	if err != nil {
		log.Println(err)
	}
}

//HelloZinxRouter Handle
type HelloZinxRouter struct {
	linet.BaseRouter
}

func (this *HelloZinxRouter) Handle(request linface.IRequest) {
	log.Println("Call HelloZinxRouter Handle")
	//先读取客户端的数据，再回写ping...ping...ping 6.3 使用Zinx-v0.9完成应用程序 90
	log.Println("recv from client : msgId=", request.GetMsgID(),
		", data=", string(request.GetData()))
	err := request.GetConnection().SendMsg(request.GetMsgID(), []byte("Hello ZinxRouter v0.9"))
	if err != nil {
		log.Println(err)
	}
}

//创建连接的时候执行
func DoConnectionBegin(conn linface.IConnection) {
	log.Println("DoConnecionBegin is Called ... ")
	//=============设置两个链接属性，在连接创建之后===========
	log.Println("Set conn Name, Home done!")
	conn.SetProperty("Name", "Aceld")
	conn.SetProperty("Home", "https://www.jianshu.com/u/35261429b7f1")
	//===================================================
	err := conn.SendMsg(2, []byte("DoConnection BEGIN..."))
	if err != nil {
		log.Println(err)
	}
}

//连接断开的时候执行
func DoConnectionLost(conn linface.IConnection) {
	//============在连接销毁之前，查询conn的Name，Home属性=====
	if name, err := conn.GetProperty("Name"); err == nil {
		log.Println("Conn Property Name = ", name)
	}
	if home, err := conn.GetProperty("Home"); err == nil {
		log.Println("Conn Property Home = ", home)
	}
	//===================================================
	log.Println("DoConneciotnLost is Called ...")
}

func main() {
	log.Println("Server starting ......")
	s := linet.NewServer("[linx v0.9]")

	//注册链接hook回调函数
	s.SetOnConnStart(DoConnectionBegin)
	s.SetOnConnStop(DoConnectionLost)

	//配置路由
	s.AddRouter(0, &PingRouter{})
	s.AddRouter(1, &HelloZinxRouter{})
	s.Serve()
}
