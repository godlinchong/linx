package main

import (
	"io"
	"linx/v0.9/linet"
	"log"
	"net"
	"time"
)

func main() {
	log.Println("Client Test Start .......")
	time.Sleep(3 * time.Second)
	conn, err := net.Dial("tcp", "localhost:4444")
	if err != nil {
		log.Println("server conn err:", err)
		return
	}
	for {
		dp := linet.NewDataPack()
		msg, _ := dp.Pack(linet.NewMsgPackage(0, []byte("Zinx v0.9Client Test Message")))
		_, err := conn.Write(msg)
		if err != nil {
			log.Println("write error err:{}", err)
			return
		}

		//先读出流中的head部分
		headData := make([]byte, dp.GetHeadLen())
		_, err = io.ReadFull(conn, headData) //ReadFull 会把msg填 充满为止
		if err != nil {
			log.Println("read head error")
			break
		}
		//将headData字节流 拆包到msg中
		msgHead, err := dp.Unpack(headData)
		if err != nil {
			log.Println("server unpack err:", err)
			return
		}
		if msgHead.GetDataLen() > 0 {
			//msg 是有data数据的，需要再次读取data数据
			msg := msgHead.(*linet.Message)
			msg.Data = make([]byte, msg.GetDataLen())
			//根据dataLen从io中读取字节流
			_, err := io.ReadFull(conn, msg.Data)
			if err != nil {
				log.Println("server unpack data err:", err)
				return
			}
			log.Println("==> Recv Msg: ID=", msg.Id, ", len=", msg.DataLen, ", data=", string(msg.Data))
		}
		time.Sleep(2 * time.Second)
	}

}
