package linet

import (
	"linx/v0.9/linface"
)

type BaseRouter struct {
}

func (br *BaseRouter) PreHandle(req linface.IRequest)  {}
func (br *BaseRouter) Handle(req linface.IRequest)     {}
func (br *BaseRouter) PostHandle(req linface.IRequest) {}
