package linet

import (
	"errors"
	"io"
	"linx/v0.9/common"
	"linx/v0.9/linface"
	"log"
	"net"
	"sync"
)

//添加读写模块交互数据的管道
type Connection struct {
	//当前Conn属于哪个Server
	TcpServer linface.IServer //当前conn属于哪个server，在conn初始化的时候添加即可
	// 当前连接的socket TCP套接字
	Conn *net.TCPConn
	// 当前连接的id
	ConnID uint32
	// 当前链接的关闭状态
	isClosed bool
	// 该连接的处理方法api
	//消息管理MsgId和对应处理方法的消息管理模块
	MsgHandler linface.IMsgHandle
	// 告知改连接已经退出或者停止的channel
	ExitBuffChan chan bool
	//无缓冲管道，用于读、写两个goroutine之间的消息通信
	msgChan chan []byte
	//有关冲管道，用于读、写两个goroutine之间的消息通信
	msgBuffChan chan []byte //定义channel成员

	// ================================
	//链接属性
	property map[string]interface{}
	//保护链接属性修改的锁
	propertyLock sync.RWMutex
	// ================================
}

// 创建连接的方法
func NewConnection(server linface.IServer, conn *net.TCPConn, connID uint32, msgHandler linface.IMsgHandle) *Connection {
	c := &Connection{
		TcpServer:    server,
		Conn:         conn,
		ConnID:       connID,
		isClosed:     false,
		MsgHandler:   msgHandler,
		ExitBuffChan: make(chan bool, 1),
		msgChan:      make(chan []byte), //msgChan初始化
		msgBuffChan:  make(chan []byte, common.GlobalObject.MaxMsgChanLen),
		property:     make(map[string]interface{}), //对链接属性ma
	}
	//将新创建的Conn添加到链接管理中
	c.TcpServer.GetConnMgr().Add(c) //将当前新创建的连接添加到Conn Manager中

	return c
}

/**
写消息goroutine 用户将数据发送给客户端
*/
func (c *Connection) StartWriter() {
	log.Println("[Writer Goroutine is running]")
	defer log.Println(c.RemoteArr().String(), "[conn writer eixt]")
	defer c.Stop()
	for {
		select {
		case data := <-c.msgChan:
			// 有数据要写给客户端
			if _, err := c.Conn.Write(data); err != nil {
				log.Println("Send Data error:", err, "Conn Writer exit")
				return
			}
			//针对有缓冲channel需要些的数据处理
		case data, ok := <-c.msgBuffChan:
			if ok {
				//有数据要写给客户端
				if _, err := c.Conn.Write(data); err != nil {
					log.Println("Send Buff Data error:, ", err, " Conn Writer exit")
					return
				}
			} else {
				break
				log.Println("msgBuffChan is Closed")
			}
		case <-c.ExitBuffChan:
			//conn已经关闭
			return

		}
	}

}

// 处理conn读取数据的Goroutine
func (c *Connection) StartReader() {
	log.Println("Reader Goroutine is running ...")
	defer log.Println(c.RemoteArr().String(), " conn reader exit!")
	defer c.Stop()
	for {
		//创建拆包解析对象
		dp := NewDataPack()
		headData := make([]byte, dp.GetHeadLen())
		if _, err := io.ReadFull(c.GetTCPConnection(), headData); err != nil {
			log.Println("read msg head error:{}", err)
			c.ExitBuffChan <- true
			continue
		}
		msg, err := dp.Unpack(headData)
		if err != nil {
			log.Println("unpack error ", err)
			c.ExitBuffChan <- true
			continue
		}
		//根据 dataLen 读取 data，放在msg.Data中
		var data []byte
		if msg.GetDataLen() > 0 {
			data = make([]byte, msg.GetDataLen())
			if _, err := io.ReadFull(c.GetTCPConnection(), data); err != nil {
				log.Println("read msg data error ", err)
				c.ExitBuffChan <- true
				continue
			}
		}
		msg.SetData(data)
		//得到当前客户端请求的Request数据
		req := Request{
			conn: c,
			msg:  msg, //将之前的buf 改成 msg
		}
		if common.GlobalObject.WorkerPoolSize > 0 {
			//已经启动工作池机制，将消息交给Worker处理
			c.MsgHandler.SendMsgToTaskQueue(&req)
		} else {
			//从绑定好的消息和对应的处理方法中执行对应的Handle方法
			go c.MsgHandler.DoMsgHandler(&req)
		}
	}
}
func (c *Connection) Start() {
	// 开启处理该连接读取到客户端数据之后的请求业务
	go c.StartReader()
	go c.StartWriter()
	//==================
	//按照用户传递进来的创建连接时需要处理的业务，执行钩子方法
	c.TcpServer.CallOnConnStart(c)
	//==================
	//for {
	select {
	case <-c.ExitBuffChan:
		//得到突出的消息不在阻塞
		return
	}
	//}

}

//获取远程客户端地址信息
func (c *Connection) RemoteArr() net.Addr {
	return c.Conn.RemoteAddr()
}

//停止连接，结束当前连接状态M
func (c *Connection) Stop() {
	log.Println("Conn Stop()...ConnID = ", c.ConnID)
	//如果当前链接已经关闭
	if c.isClosed == true {
		return
	}
	c.isClosed = true

	//==================
	//如果用户注册了该链接的关闭回调业务，那么在此刻应该显示调用
	c.TcpServer.CallOnConnStop(c)
	//==================

	// 关闭socket链接
	_ = c.Conn.Close()
	// 通知从缓冲队列读取数据的业务,该连接该链接已关闭
	c.ExitBuffChan <- true
	//将链接从连接管理器中删除
	c.TcpServer.GetConnMgr().Remove(c) //删除conn从ConnManager中
	// 关闭改连接的全部管道
	close(c.ExitBuffChan)
	close(c.msgChan)
}

//从当前链接获取原始的socket TCPConn
func (c *Connection) GetTCPConnection() *net.TCPConn {
	return c.Conn
}

func (c *Connection) GetConnID() uint32 {
	return c.ConnID
}

//直接将Message数据发送数据给远程的TCP客户端
func (c *Connection) SendMsg(msgId uint32, data []byte) error {
	return c.send(msgId, data, true)
}

//直接将Message数据发送数据给远程的TCP客户端
/**
flag 默认表示不用缓存通道发送
*/
func (c *Connection) send(msgId uint32, data []byte, flag bool) error {
	if c.isClosed == true {
		return errors.New("Connection closed when send msg")
	}
	//将data封包，并且发送
	dp := NewDataPack()
	msg, err := dp.Pack(NewMsgPackage(msgId, data))
	if err != nil {
		log.Println("Pack error msg id = ", msgId)
		return errors.New("Pack error msg ")
	}
	// 写回客户端
	if flag {
		c.msgChan <- msg
	} else {
		c.msgBuffChan <- msg
	}
	return nil
}

func (c *Connection) SendBuffMsg(msgId uint32, data []byte) error {
	return c.send(msgId, data, false)
}

func (c *Connection) SetProperty(key string, value interface{}) {
	c.propertyLock.Lock()
	defer c.propertyLock.Unlock()
	c.property[key] = value
}

//获取链接属性
func (c *Connection) GetProperty(key string) (interface{}, error) {
	c.propertyLock.RLock()
	defer c.propertyLock.RUnlock()
	if value, ok := c.property[key]; ok {
		return value, nil
	} else {
		return nil, errors.New("no property found")
	}
}

//移除链接属性
func (c *Connection) RemoveProperty(key string) {
	c.propertyLock.Lock()
	defer c.propertyLock.Unlock()
	delete(c.property, key)
}
