package linet

import (
	"linx/v0.9/linface"
)

type Request struct {
	conn linface.IConnection // 已经和客户端建立好的链接
	msg  linface.IMessage
}

func (r *Request) GetConnection() linface.IConnection {
	return r.conn
}

//获取请求消息的数据
func (r *Request) GetData() []byte {
	return r.msg.GetData()
}

//获取请求的消息的ID
func (r *Request) GetMsgID() uint32 {
	return r.msg.GetMsgId()
}
