package linet

import (
	"errors"
	"fmt"
	"linx/v0.7/common"
	"linx/v0.7/linface"
	"log"
	"net"
	"time"
)

// iServer 接口实现，定义一个server服务类
type Server struct {
	// 名称
	Name string
	// tcp4 or tcp6
	IPVersion string
	//ip
	IP string
	// port
	Port int
	//当前Server的消息管理模块，用来绑定MsgId和对应的处理方法
	msgHandler linface.IMsgHandle
}

func (s *Server) Start() {
	fmt.Printf("[Start] Server listenner at IP: "+
		"%s,Port %d is staring\n", s.IP, s.Port)
	// 开启一个gorouties去做监听
	go func() {
		//0 启动worker工作池机制
		s.msgHandler.StartWorkerPool()
		// 1 获取一个tcp的Addr
		addr, err := net.ResolveTCPAddr(s.IPVersion, fmt.Sprintf("%s:%d", s.IP, s.Port))
		common.CheckErr(err, "resolve tcp addr err:")
		// 监听服务器地址
		listenner, err := net.ListenTCP(s.IPVersion, addr)
		common.CheckErr(err, "listen TCP err:")
		// 2.已经成功监控
		fmt.Println("start Linx Server ", s.Name, " succ,now listening ...")
		// 3.启动server网络连接业务
		// ConnID生成
		var cid uint32
		cid = 0
		for {
			// 阻塞等待客户端建立连接请求
			conn, err := listenner.AcceptTCP()
			if err != nil {
				fmt.Println("Accept err ", err)
				continue
			}
			// 处理连接的业务请求方法,此时应该有handler 和 conn是绑定的
			dealConn := NewConnection(conn, cid, s.msgHandler)
			cid++
			// 启动当前链接的处理业务
			go dealConn.Start()

		}
	}()
}

func (s *Server) Stop() {
	fmt.Println("[Stop] Linx Server, name:", s.Name)
}

func (s *Server) Serve() {
	s.Start()
	for {
		time.Sleep(10 * time.Second)
	}

}

func (s *Server) Status() {
	fmt.Println("TDD status")
}

func (s *Server) AddRouter(msgId uint32, router linface.IRouter) {
	s.msgHandler.AddRouter(msgId, router)
	fmt.Println("msgId:", msgId, ",router:", router)
}

func NewServer(name string) linface.IServer {
	s := &Server{
		Name:       name,
		IPVersion:  "tcp4",
		IP:         "localhost",
		Port:       4444,
		msgHandler: NewMsgHandle(),
	}
	return s
}

// ========= 定义当前客户端链接的handle api==============
func CallBackToClient(conn *net.TCPConn, data []byte, cnt int) error {
	// 回显业务
	log.Println("[Conn Handle] CallBackToClient")
	if _, err := conn.Write(data[:cnt]); err != nil {
		log.Println("Write back buf err", err)
		return errors.New("CallBackToClient error")
	}
	return nil
}
