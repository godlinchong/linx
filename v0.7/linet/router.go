package linet

import (
	"linx/v0.7/linface"
)

type BaseRouter struct {
}

func (br *BaseRouter) PreHandle(req linface.IRequest)  {}
func (br *BaseRouter) Handle(req linface.IRequest)     {}
func (br *BaseRouter) PostHandle(req linface.IRequest) {}
