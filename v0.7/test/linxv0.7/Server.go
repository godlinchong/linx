package main

import (
	"fmt"
	"io"
	"linx/v0.7/linet"
	"net"
)

func main() {
	listener, err := net.Listen("tcp", "localhost:4444")
	if err != nil {
		fmt.Println("server listen err:", err)
	}
	//创建服务器gotoutine，负责从客户端goroutine读取粘包的数据，然后进行解析
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("server accept err:", err)
		}
		// 处理客户端请求
		go func(conn net.Conn) {
			dp := linet.NewDataPack()
			for {
				//1 先读出流中的head部分
				headData := make([]byte, dp.GetHeadLen())
				_, err := io.ReadFull(conn, headData) //ReadFull会把msg填充满为止
				if err != nil {
					fmt.Println("read head error")
					break
				}
				//将headData字节流 拆包到msg中
				msgHead, err := dp.Unpack(headData)
				if err != nil {
					fmt.Println("server unpack err:", err)
					return
				}
				if msgHead.GetDataLen() > 0 {
					//msg有数据需要再次读取data的数据
					msg := msgHead.(*linet.Message)
					msg.Data = make([]byte, msgHead.GetDataLen())
					// 根据datalen从io中读取字节流数据
					_, err := io.ReadFull(conn, msg.Data)
					if err != nil {
						fmt.Println("server unpack data err:", err)
						return
					}
					fmt.Println("===> recv Msg:Id=", msg.Id, ",len=", msg.DataLen, ",data=", string(msg.Data))
				}
			}
		}(conn)
	}

}
